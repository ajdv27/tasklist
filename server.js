//require Express and assign it to the variable called express
const express = require('express');

//call mongoose for handling multiple db connections

const mongoose = require("mongoose");

//call body parsin for json content types
const bodyParser = require("body-parser");

//include cors to allow cross-site http requests
const cors = require("cors");


//include jsonwebtoken for us to create a JWT
const jwt = require("jsonwebtoken");

//load passport for authentication:
const passport = require("passport");

//load our custom passport.js
const appPassport = require("./passport");

//use bcrypt to be able to encrypt passwords:
const bcrypt = require("bcrypt-nodejs");


//establish a connection to the database:
//mongoose.connect("mongodb://localhost/sampledb");
mongoose.connect("mongodb+srv://database-admin:testing1234@sampledb-thman.mongodb.net/pets?retryWrites=true");

//pet model:
const PetModel = require("./models/Pet.js");

//require the fruit model
const FruitModel = require("./models/Fruit.js");

//task model
const TaskModel = require("./models/Task.js");

//member model
const MemberModel = require("./models/Member.js");

//Invoke express and assign the return value to the variable called app.
const app = express();

//use cors
app.use(cors());

//save the value 3000 to the variable called port.
const port = 3000;




//use body parser in our application
app.use(bodyParser.json());

//in app, use the method called 'listen'
app.listen(port, () => console.log(`The server is listening to port ${port}`));

//get method
app.get('/', function (req, res, next) {
	//res.send('Hello World!')
	//res.send({data:1});
	next();
});

app.get('/', function (req, res) {
	//res.send('Hello World!')
	res.send({data:2});
});

//post request method
app.post('/', function (req, res) {
	res.send('Got a POST request')
});

app.put('/tuittcamper', function (req, res) {
	res.send('Got a PUT request from /tuittcamper!')
});

app.delete('/tuittcamper', function (req, res) {
	res.send('Got a DELETE request from /tuittcamper!')
});


//fruits route
app.get("/marketplace", function(req, res){
	FruitModel.find({}, (err, items) => {
		if(!err){
			return res.json({
				"items":items
			})
		} else {
			console.log(err);
		}
	});
});

app.get("/marketplace/:id", function(req, res){
	FruitModel.findOne({"_id" : req.params.id}) //req.params -> any wildcard we placed in the URL
	.then((fruit) => {
		if(fruit){
			return res.json({
				"result": fruit
			});
		} else {
			return res.send("No fruit found");
		}
	})
});

app.post("/fruit/add", function(req, res){
	//return res.send(req.body);
	FruitModel.create(req.body).then(function (result){
		return res.json(result);
	})
});

app.delete("/fruit/delete/:id", function(req, res){
	FruitModel.deleteOne({"_id":req.params.id})
	.then(fruit=> {
		if(fruit){
			return res.json({
				"fruit": fruit
			})
		}

		return res.json({
			"message":"No Fruit found"
		})
	})
});

app.put("/fruit/edit/:id", function(req, res){
	//id -> to  know what item to modify
	//body -> to know what data to modify with
	
	FruitModel.update({"_id":req.params.id}, req.body)
	.then(fruit => {
		if(fruit){
			return res.json({
				"result": fruit
			})
		}
		return res.json({"message": "no fruit found"});
	})
});

//pets route
app.get("/petplace", function(req, res){
	PetModel.find({}, (err, animals) => {
		if(!err){
			return res.json({
				"animals":animals
			})
		} else {
			console.log(err);
		}
	});
});

app.get("petplace/:id", function(req, res){
	PetModel.findOne({"_id" : req.params.id})
	.then((pet) => {
		if(pet){
			return res.json({
				"result": pet
			});
		} else {
			return res.send("No pet found");
		}
	})
});

app.post("/pet/add", function(req, res){
	PetModel.create(req.body).then(function (result){
		return res.json(result);
	})
});

app.put("/pet/edit/:id", function(req, res){
	PetModel.update({"_id":req.params.id}, req.body)
	.then(pet => {
		if(pet){
			return res.json({
				"result": pet
			})
		}
		return res.json({"message": "no pet found"});
	})
}); 

app.delete("/pet/delete/:id", function(req, res){
	PetModel.deleteOne({"_id":req.params.id})
	.then(pet=> {
		if(pet){
			return res.json({
				"pet": pet
			})
		}
		return res.json({
			"message": "No Pet found"
		})
	})
});

// //friend routes
const friend = require("./routes/rentFriend");
//app.use("/friend", friend);
app.use("/friend", passport.authenticate("jwt", {session:false}), friend);

// //all the routes that have a word friend will look at the routes specified in rentFriend.js

//user route
const user = require("./routes/admin");
app.use("/admin", user);

//sample route
const sample = require("./routes/sample");
app.use("/sample", sample);

//task route
const task = require("./routes/tasklist");
//app.use("/task", task);
app.use("/task", passport.authenticate("jwt", {session:false}), task);


const someLogger = (req, res, next) => {
	if(req.body.age >= 18){
		console.log("I'm a Logger");
		next();
	} else {
		res.send("you are underage");
	}
}

app.get("/middletest", someLogger ,function(req, res){
	res.send("You are of legal age");
})

app.post("/login", function(req, res){
	const user = {
		"_id" : 1,
		"user" : "brandon",
		"email" : "brandon@mail.com"

	}
	//"secretb27" is the secret to create the signature
	jwt.sign({"user" : user}, "secretb27", (err, token) => {
		return res.json({
			"token" : token
		})
	});
});

const verifyToken = (req, res, next) => {
	const header = req.headers['authorization'];
	if(typeof header !== 'undefined'){
		//prase the token 
		req.token = header.slice(7, header.length);
		//we need to slice this because we want to revmoe the word "bearer"
		next();
	} else {
		res.sendStatus(403); //forbidden
	}
}

app.post("/post/create", verifyToken, function(req, res){
	//we get the token cleaned up by verifyToken and verify it.
	jwt.verify(req.token, "secretb27", (err, data) => {
		if (err) {
			res.sendStatus(403);
		} else {
			res.json({
				"message" : "new post created",
				"authorizedData" : data
			})
		}
	})
})

app.post("/member/register", function(req, res){
	bcrypt.genSalt(10, function(err, salt){
		bcrypt.hash(req.body.password, salt, null, function(err, hash){
			MemberModel.create({
				"email": req.body.email,
				"password": hash,
				"role": "user"
			}).then((user)=> {
				return res.status(200).send(user);
			})
		})
	})
	

})

app.post("/member/login", function(req, res, next){
	passport.authenticate("local", {session: false}, (err, user, info)=>{
		if(user){
			let token = jwt.sign(user.toJSON(), "secretb27");
			return res.status(200).json({
				"data": {
					"user": user,
					"token": token
				}
			})
		} else {
			return res.send("error");
		}
	}) (req, res);
});


app.get("/protected", passport.authenticate("jwt", {session:false}), function (req, res)
	{
	return res.send("This can only be accessed by a regular user")
})