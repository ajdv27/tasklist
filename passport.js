//call passport to be able to use passport

const passport = require("passport");

//use passportJWT for the authentication of the web token
const passportJWT = require("passport-jwt");

//load JWT extraction to be able to extract tokens later on
const JWTExtractor = passportJWT.ExtractJwt;

//use passport-local for authentication using username and password
const passportLocal = require("passport-local");

//creation of local and jwt strategies
const LocalStrategy = passportLocal.Strategy;
const JWTStrategy = passportJWT.Strategy;

//include the member model
const MemberModel = require("./models/Member");

//Include bcrypt library for encryption
const bcrypt = require("bcrypt-nodejs");
//----- end of required packages -----//


//prepare the daya for the user for the entire session
//Serialization -> object to json (or any other format that can be transferred)
passport.serializeUser((user, done) => {
	if(!user._id){
		user._id = user.sub; //subject from the payload
	}
	done(null, user._id);
});

//how to reread the prepped data
//Deserialization -> json to object (how to recover the object given its json equivalent)
passport.deserializeUser((id, done) => {
	done(null, user._id);
});

//configure passport.js to use the local strategy
passport.use(new LocalStrategy({usernameField: "email"}, function(email, password, done)
	{
	//Query for a member that matches the email provided
	MemberModel.findOne({"email" : email}).then(function(user){
		//uncomment this to view the user that is being checked
		//console.log(user);
	
	if(!user){
		return done(null, false, {"message" : "Invalid Credentials"});
	}

	if(email == user.email){
		//check if the password hash is same with stored password
		if(!bcrypt.compareSync(password, user.password)) {
			return done(null, false, {"message" : "Invalid Credentials"});
		} else {
			return done(null, user); //returns a successful login if a match is found
		}
	}
	
	return done(null, false, {"message" : "Invalid Credentials"});

	});

	
}));

//configure passport for JWT usage
passport.use(new JWTStrategy({
	jwtFromRequest: JWTExtractor.fromAuthHeaderAsBearerToken(),
	secretOrKey: "secretb27"
}, function(payload, next){
	console.log(payload);
	return next(null, payload);
}
));

