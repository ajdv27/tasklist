//load mongoose to be able to use models

const mongoose = require("mongoose");

//use mongoose's schema for the models
const Schema = mongoose.Schema;

//create a new Schema for fruits
const FruitSchema = new Schema({
	name: String,
	price: Number,
	color: String,
	taste: String,
	expiryDate: {
		year: Number,
		month: Number
	}
}); 

//export the model so that other files can use it.
module.exports = mongoose.model("Fruit", FruitSchema);

