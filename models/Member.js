const mongoose = require("mongoose");

//load jsonwebtoken for authentication
const jwt = require("jsonwebtoken");


const Schema = mongoose.Schema;

const MemberSchema = new Schema({
	email: String,
	password: String,
	role: String
})


module.exports = mongoose.model("Member", MemberSchema);