const mongoose = require("mongoose");

const jwt = require("jsonwebtoken");

const Schema = mongoose.Schema;

const TaskSchema = new Schema ({
	name: String,
	status: String,
});

module.exports = mongoose.model("Task", TaskSchema);