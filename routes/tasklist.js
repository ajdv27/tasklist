const express = require("express");

const router = express.Router();

const TaskModel = require("../models/Task.js");


//show tasks
router.get("/", function(req, res) {
	TaskModel.find({}, (err, task) => {
		if(!err){
			return res.json({"task": task});
		} else {
			console.log(err);
		}
	})
});

//add task
router.post("/add", function(req, res){
	TaskModel.create(req.body).then(function(result){
		return res.send(result);
	});
});

//edit task
router.put("/:id", function(req, res){
	TaskModel.update({"_id":req.params.id}, req.body)
	.then(task => {
		if(task){
			return res.json({
				"result": task
			})
		}
		return res.json({"task": "no task found"});
	})
});

//delete task
router.delete("/:id", function(req, res){
	TaskModel.deleteOne({"_id":req.params.id})
	.then(task=> {
		if(task){
			return res.json({
				"task": task
			})
		}
		return res.json({
			"message": "no task found"
		})
	})
});

module.exports = router;