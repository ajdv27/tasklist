const express = require("express");

const router = express.Router();

const FriendModel = require("../models/Friends.js");

//find all items
router.get("/", function(req, res) {
	FriendModel.find({}, (err, friends) => {
		if(!err){
			return res.json({"friends": friends});
		} else {
			console.log(err);
		}
	})
});

//make a friend
router.post("/add", function(req, res){
	FriendModel.create(req.body).then(function(result){
		return res.send(result);
	});
});


router.get("/:id", function(req, res){
	FriendModel.findOne({"_id" : req.params.id})
	.then((friend) => {
		if(friend){
			return res.json({
				"result": friend
			});
		} else {
			return res.send("No friend found");
		}
	})
});

router.put("/:id", function(req, res){
	FriendModel.update({"_id":req.params.id}, req.body)
	.then(friend => {
		if(friend){
			return res.json({
				"result": friend
			})
		}
		return res.json({"friend": "no friend found"});
	})
});

router.delete("/:id", function(req, res){
	FriendModel.deleteOne({"_id":req.params.id})
	.then(friend=> {
		if(friend){
			return res.json({
				"friend": friend
			})
		}
		return res.json({
			"message": "No friend found"
		})
	})
});



module.exports = router;