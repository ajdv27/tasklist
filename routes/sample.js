const express = require("express");

const router = express.Router();

router.get("/", function (req, res){
	return res.json({"message" : "Greetings from the backend"});
});

router.get("/greet", function(req, res){
	return res.json({"message": "Hello World"});
});

module.exports = router
